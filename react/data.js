export const paymentMethodGroups = {
  creditCardPaymentGroup: "Tarjeta de crédito",
  debitCardPaymentGroup: "Tarjeta de débito",
  cardPromissoryPaymentGroup: "Tarjeta promisoria",
  MercadoPagoPaymentGroup: "Mercado Pago",
  bankInvoicePaymentGroup: "Pago en efectivo",
  customPrivate_501PaymentGroup: "Tarjeta Cencosud",
  customPrivate_502PaymentGroup: "Tarjeta Cordobesa",
  customPrivate_503PaymentGroup: "Tarjeta CMR",
  customPrivate_504PaymentGroup: "Tarjeta Cordial",
  custom201PaymentGroupPaymentGroup: "Transferencia bancaria",
  custom202PaymentGroupPaymentGroup: "Pago en sucursal"

}
export const defaultValues = {
  creditCardPaymentGroup: "Visa",
  debitCardPaymentGroup: "Visa Electron",
  cardPromissoryPaymentGroup: "CardPromissory",
  MercadoPagoPaymentGroup: "Mercado Pago",
  bankInvoicePaymentGroup: "RAPIPAGO",
  customPrivate_501PaymentGroup: "Tarjeta Cencosud",
  customPrivate_502PaymentGroup: "Tarjeta Cordobesa",
  customPrivate_503PaymentGroup: "Tarjeta CMR",
  customPrivate_504PaymentGroup: "Tarjeta Cordial",
  //Estos valores hay que ver cómo vienen en la info para cambiar los datos en el front
  custom201PaymentGroupPaymentGroup: "Transferencia bancaria",
  custom202PaymentGroupPaymentGroup: "Pago en sucursal"
}

export const paymentOptionsIcons = {
  'Visa': 'https://innew.vteximg.com.br/arquivos/icono-visa-modal.png',
  'Mastercard': 'https://innew.vteximg.com.br/arquivos/icono-mastercard-modal.png',
  'Nativa': 'https://innew.vteximg.com.br/arquivos/icono-nativa-modal.png',
  'Naranja': 'https://innew.vteximg.com.br/arquivos/icono-naranja-modal.png',
  'Visa Electron': 'https://innew.vteximg.com.br/arquivos/logo-visaelectron-modal.png',
  'Mercado Pago': 'https://innew.vteximg.com.br/arquivos/logo-mp-modal.png',
  'CardPromissory': 'https://innew.vteximg.com.br/arquivos/icono-tarjeta-modal.png',
  'Maestro': 'https://innew.vteximg.com.br/arquivos/logo-maestro-modal.png',
  'American Express': 'https://innew.vteximg.com.br/arquivos/logo-american-modal.png',
  'PAGOFACIL': 'https://innew.vteximg.com.br/arquivos/logo-pagofacil-modal.png',
  'RAPIPAGO': 'https://innew.vteximg.com.br/arquivos/logo-rapipago-modal.png',
  'Tarjeta Cencosud': 'https://innew.vteximg.com.br/arquivos/logo-cencosud-modal.png',
  'Tarjeta Cordobesa': 'https://innew.vteximg.com.br/arquivos/logo-cordobesa-modal.png',
  'Tarjeta CMR': 'https://innew.vteximg.com.br/arquivos/logo-cmr-modal.png',
  'Tarjeta Cordial': 'https://innew.vteximg.com.br/arquivos/logo-cordial-modal.png'


}
